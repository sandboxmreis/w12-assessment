﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using W12.Assessment.Matheus.Domain.Core.Entities;

namespace W12.Assessment.Matheus.Domain.Entities
{
    public class Contact : Entity
    {
        public Contact()
        {
            
        }
        public Contact(string name, string telephone, Guid id)
        {
            Name = name;
            Telephone = telephone;
            Id = id;
        }

        public string Name { get; private set; }
        public string Telephone { get; private set; }
    }
}
