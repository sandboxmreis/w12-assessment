﻿using MediatR;
using W12.Assessment.Matheus.Domain.Core.Bus;
using W12.Assessment.Matheus.Domain.Core.Commands;
using W12.Assessment.Matheus.Domain.Core.Notifications;
using W12.Assessment.Matheus.Domain.Interfaces;

namespace W12.Assessment.Matheus.Domain.CommandHandlers
{
    public class CommandHandler
    {
        private readonly IUnitOfWork _uow;
        private readonly IMediatorHandler _bus;
        private readonly DomainNotificationHandler _notifications;

        public CommandHandler(IUnitOfWork uow, IMediatorHandler bus, INotificationHandler<DomainNotification> notifications)
        {
            _uow = uow;
            _notifications = (DomainNotificationHandler)notifications;
            _bus = bus;
        }

        protected void NotifyValidationErrors(Command message)
        {
            foreach (var error in message.ValidationResult.Errors)
            {
                _bus.RaiseEvent(new DomainNotification(message.MessageType, error.ErrorMessage));
            }
        }

        public bool Commit()
        {
            if (_notifications.HasNotifications())
            {
                _bus.RaiseEvent(new DomainNotification("Commit", "We had a problem during saving your data."));
                return false;
            }
            var commandResponse = _uow.Commit();
            if (commandResponse.Success)
                return true;
            return false;
        }
    }
}
