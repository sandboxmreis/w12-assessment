﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using W12.Assessment.Matheus.Domain.Commands;
using W12.Assessment.Matheus.Domain.Core.Bus;
using W12.Assessment.Matheus.Domain.Core.Notifications;
using W12.Assessment.Matheus.Domain.Entities;
using W12.Assessment.Matheus.Domain.Interfaces;

namespace W12.Assessment.Matheus.Domain.CommandHandlers
{
    public class ContactCommandHandler : CommandHandler,
        INotificationHandler<CreateContactCommand>,
        INotificationHandler<DeleteContactCommand>
    {
        private readonly IContactRepository _contactRepository;

        private readonly IMediatorHandler Bus;
        public ContactCommandHandler(IMediatorHandler bus, IUnitOfWork uow, IContactRepository contactRepository,
                                    INotificationHandler<DomainNotification> notifications) : base(uow, bus, notifications)
        {
            _contactRepository = contactRepository;
            Bus = bus;
        }

        public void Handle(CreateContactCommand message)
        {
            if (!message.IsValid())
            {
                NotifyValidationErrors(message);
                return;
            }
            var contact = new Contact(message.Name, message.Telephone, Guid.NewGuid());
            _contactRepository.Add(contact);

            if (Commit())
            {
                //Caso queira enviar alguma notificação da geração de um novo contato, ou salvar no banco como log.
                //Bus.RaiseEvent();
            }

        }

        public void Handle(DeleteContactCommand message)
        {
            if (!message.IsValid())
            {
                NotifyValidationErrors(message); 
            }
            _contactRepository.Delete(message.Id);
            Commit();
             
 
        }
    }
}
