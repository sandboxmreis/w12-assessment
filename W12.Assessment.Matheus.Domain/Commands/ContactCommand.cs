﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using W12.Assessment.Matheus.Domain.Core.Commands;

namespace W12.Assessment.Matheus.Domain.Commands
{
    public abstract class ContactCommand : Command
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Telephone { get; set; }
    }
}
