﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using W12.Assessment.Matheus.Domain.Validations;

namespace W12.Assessment.Matheus.Domain.Commands
{
    public class CreateContactCommand : ContactCommand
    {
        public CreateContactCommand(string name, string telephone)
        {
            Name = name;
            Telephone = telephone;
        }
        public override bool IsValid()
        {
            ValidationResult = new CreateContactValidation().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}
