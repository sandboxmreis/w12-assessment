﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using W12.Assessment.Matheus.Domain.Validations;

namespace W12.Assessment.Matheus.Domain.Commands
{
    public class DeleteContactCommand : ContactCommand
    {
        public DeleteContactCommand(Guid id)
        {
            Id=id;
        }
        public override bool IsValid()
        {
            ValidationResult = new DeleteContactValidation().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}
