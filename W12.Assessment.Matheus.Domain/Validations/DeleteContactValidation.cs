﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using W12.Assessment.Matheus.Domain.Commands;

namespace W12.Assessment.Matheus.Domain.Validations
{
    public class DeleteContactValidation : ContactValidation<ContactCommand>
    {
        public DeleteContactValidation()
        {
            ValidateId();
        }
    }
}
