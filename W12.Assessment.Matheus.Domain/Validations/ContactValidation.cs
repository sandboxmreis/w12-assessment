﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using W12.Assessment.Matheus.Domain.Commands;

namespace W12.Assessment.Matheus.Domain.Validations
{
    public class ContactValidation<T> : AbstractValidator<T> where T:ContactCommand
    {
        protected void ValidateName()
        {
            RuleFor(c => c.Name)
                .NotEmpty().WithMessage("Por favor, informe o Nome.")
                .Length(2, 150).WithMessage("O Nome deve ter entre 2 e 150 caractéres.");
        }
        protected void ValidateTelephone()
        {
            RuleFor(c => c.Telephone)
              .NotEmpty().WithMessage("Por favor, informe o Telefone.")
              .Length(10, 11).WithMessage("O Telefone deve ter entre 10 e 11 caractéres.");
        }
        protected void ValidateId(){
             RuleFor(c => c.Id)
                .NotEqual(Guid.Empty);
        }
    }
}
