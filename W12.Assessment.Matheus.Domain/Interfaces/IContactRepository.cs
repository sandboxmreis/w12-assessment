﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using W12.Assessment.Matheus.Domain.Entities;

namespace W12.Assessment.Matheus.Domain.Interfaces
{
    public interface IContactRepository : IRepository<Contact>
    {
         
    }
}
