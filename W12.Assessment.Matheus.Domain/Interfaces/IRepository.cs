﻿using System;
using System.Linq;

namespace W12.Assessment.Matheus.Domain.Interfaces
{
    public interface IRepository<TEntity> : IDisposable where TEntity : class
    {
        void Add(TEntity obj);
        void Delete(Guid id);
        IQueryable<TEntity> GetAll();
        
        int SaveChanges();
    }
}
