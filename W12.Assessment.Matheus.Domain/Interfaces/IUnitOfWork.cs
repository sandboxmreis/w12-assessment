﻿using System;
using W12.Assessment.Matheus.Domain.Core.Commands;

namespace W12.Assessment.Matheus.Domain.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        CommandResponse Commit();
    }
}
