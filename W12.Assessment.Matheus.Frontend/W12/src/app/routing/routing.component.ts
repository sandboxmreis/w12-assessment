import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from '../app.component';
import { ContactListComponent } from '../contact-list/contact-list.component';
import { NewContactComponent } from '../new-contact/new-contact.component';

const appRoutes: Routes = [
  { path: 'listContacts', component: ContactListComponent },
  { path: 'newContact', component: NewContactComponent },
  { path: '**', component: ContactListComponent },
];

export const routing = RouterModule.forRoot(appRoutes);
