import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatCardModule, MatMenuModule, MatDialogModule,
        MatToolbarModule, MatIconModule, MatTableModule,MatInputModule } from '@angular/material';
import { AppComponent } from './app.component';
import { ContactService } from './Service/contact.service';
import { ContactListComponent } from './contact-list/contact-list.component';
import { NewContactComponent } from './new-contact/new-contact.component'; 
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { routing } from './routing/routing.component';
import { DialogDefaultComponent } from './dialog-default/dialog-default.component';     

@NgModule({
  declarations: [
    AppComponent,
    ContactListComponent,
    NewContactComponent,
    DialogDefaultComponent,
  ],
  imports: [

    BrowserModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatInputModule,
    MatTableModule,
    MatButtonModule,
    MatMenuModule,
    MatCardModule,
    MatToolbarModule,
    MatIconModule, 
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    routing 
  ],
  providers: [ContactService ],
  bootstrap: [AppComponent],
  entryComponents: [DialogDefaultComponent]

})
export class AppModule { }
