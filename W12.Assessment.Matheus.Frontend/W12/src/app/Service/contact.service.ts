import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response, RequestMethod } from '@angular/http';
import { IContact } from '../Models/IContact';
 
@Injectable()
export class ContactService {

  _baseUrl: string = "http://localhost:5000/";
  contactList: IContact[]; 
  constructor(private http: Http) {  
    
  }
  public getAll() {
    return this.http.get(this._baseUrl + "contacts").toPromise();
  }
  public add(contact: IContact) {
    return this.http.post(this._baseUrl + "contacts", contact).toPromise();
  }
  public delete(id: string) {
    return this.http.delete(this._baseUrl + "contacts/?id=" + id).toPromise();
  }
}
