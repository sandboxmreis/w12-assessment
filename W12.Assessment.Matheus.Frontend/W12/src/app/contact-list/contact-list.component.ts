import { Component, OnInit } from '@angular/core';
import { ContactService } from '../Service/contact.service';
import { IContact } from '../Models/IContact';
import { MatDialog } from '@angular/material';
import { DialogDefaultComponent } from '../dialog-default/dialog-default.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.css']
})
export class ContactListComponent implements OnInit {

  public listContacts: any;
  public listContactsData: IContact[];
  columnsToDisplay = ['id','name', 'telephone','actions'];

  constructor(private _contactService: ContactService, public dialog: MatDialog ,private router: Router) {
    this.init()
  }

  private init() {
    this._contactService.getAll()
      .then(response => {
        this.listContacts = response.json(),
        this.listContactsData=this.listContacts.data 
      });
  }
  public deleteContact(id: string) {
    this._contactService.delete(id)
      .then(resp=>
        this.openDialog(true,"Contato excluído com sucesso."))
      .catch((resp) => {
        this.openDialog(false,resp.json().errors)
      });
      this.init();
  }
  btnClick = function () {
    this.router.navigateByUrl('/newContact');
  };
  openDialog(success: boolean, message: string) {
    this.dialog.open(DialogDefaultComponent, {
      data: {
        result: success,
        message: message
      }
    });
  }
  ngOnInit() {

  }

}
