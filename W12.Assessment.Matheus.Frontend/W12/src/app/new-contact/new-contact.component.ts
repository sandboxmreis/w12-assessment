import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ContactService } from '../Service/contact.service';
import { validateConfig } from '@angular/router/src/config';
import { IContact } from '../Models/IContact';
import { MatDialog } from '@angular/material';
import { DialogDefaultComponent } from '../dialog-default/dialog-default.component';

@Component({
  selector: 'app-new-contact',
  templateUrl: './new-contact.component.html',
  styleUrls: ['./new-contact.component.css']
})
export class NewContactComponent implements OnInit {
  complexForm: FormGroup;
  constructor(fb: FormBuilder, private _contactService: ContactService, public dialog: MatDialog) {
    this.complexForm = fb.group({
      'name': [null, Validators.required],
      'telephone': [null, Validators.required]
    });
  }

  ngOnInit() {
  }
  public newContact(model: IContact) {
    this._contactService.add(model)
      .then(resp => {
        this.openDialog(true,"Contato inserido com sucesso.")
      }).catch(exp => {
        this.openDialog(false,exp.json().errors)
      });
  }
  openDialog(success: boolean, message: string) {
    this.dialog.open(DialogDefaultComponent, {
      data: {
        result: success,
        message: message
      }
    });

  }
}
