import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material';
@Component({
  selector: 'app-dialog-default',
  templateUrl: './dialog-default.component.html',
  styleUrls: ['./dialog-default.component.css']
})
export class DialogDefaultComponent  {

  constructor(@Inject(MAT_DIALOG_DATA) public result: any) { }
 
}
