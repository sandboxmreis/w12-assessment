﻿using FluentValidation.Results;
using System;
using W12.Assessment.Matheus.Domain.Core.Events;

namespace W12.Assessment.Matheus.Domain.Core.Commands
{
    public abstract class Command  : Message
    { 
        public DateTime Timestamp { get; private set; }
         public ValidationResult ValidationResult { get; set; }

        protected Command()
        {
            Timestamp = DateTime.Now;
        }

        public abstract bool IsValid();
    }
}
