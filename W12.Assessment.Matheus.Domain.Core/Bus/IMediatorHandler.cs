﻿using System.Threading.Tasks;
using W12.Assessment.Matheus.Domain.Core.Commands;
using W12.Assessment.Matheus.Domain.Core.Events;

namespace W12.Assessment.Matheus.Domain.Core.Bus
{
    public interface IMediatorHandler
    {
        Task SendCommand<T>(T command) where T : Command;
        Task RaiseEvent<T>(T @event) where T : Event;
    }
}
