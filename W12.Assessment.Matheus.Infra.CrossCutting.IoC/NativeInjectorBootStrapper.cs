﻿using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using W12.Assessment.Matheus.Application.Interfaces;
using W12.Assessment.Matheus.Application.Services;
using W12.Assessment.Matheus.Domain.CommandHandlers;
using W12.Assessment.Matheus.Domain.Commands;
using W12.Assessment.Matheus.Domain.Core.Bus;
using W12.Assessment.Matheus.Domain.Core.Notifications;
using W12.Assessment.Matheus.Domain.Interfaces;
using W12.Assessment.Matheus.Infra.CrossCutting.Bus;
using W12.Assessment.Matheus.Infra.Data.Context;
using W12.Assessment.Matheus.Infra.Data.Repository;
using W12.Assessment.Matheus.Infra.Data.UoW;
using Microsoft.AspNetCore.Http;
using AutoMapper;
using W12.Assessment.Matheus.Application.AutoMapper;

namespace W12.Assessment.Matheus.Infra.CrossCutting.IoC
{
    public class NativeInjectorBootStrapper
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();


            // Domain Bus (Mediator)
            services.AddScoped<IMediatorHandler, InMemoryBus>();

            // Application
            services.AddSingleton<IConfigurationProvider>(AutoMapperConfig.RegisterMappings());
            services.AddScoped<IMapper>(sp => new Mapper(sp.GetRequiredService<AutoMapper.IConfigurationProvider>(), sp.GetService));

            services.AddScoped<IContactService, ContactService>();

            // Domain - Events
            services.AddScoped<INotificationHandler<DomainNotification>, DomainNotificationHandler>();

            // Domain - Commands
            services.AddScoped<INotificationHandler<CreateContactCommand>, ContactCommandHandler>();
            services.AddScoped<INotificationHandler<DeleteContactCommand>, ContactCommandHandler>();

            // Infra - Data
            services.AddScoped<IContactRepository, ContactRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<W12Context>();
        }
    }
}
