﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using W12.Assessment.Matheus.Application.Interfaces;
using W12.Assessment.Matheus.Application.ViewModels;
using W12.Assessment.Matheus.Domain.Core.Notifications;

namespace W12.Assesment.Matheus.WebAPI.Controllers
{
    public class ContactsController : BaseApiController
    {
        private readonly IContactService _contactService;
        public ContactsController(IContactService contactService,
                                        INotificationHandler<DomainNotification> notifications) : base(notifications)
        {
            _contactService = contactService;
        }
        [HttpGet]
        [Route("contacts")]
        public IActionResult Get()
        {
            return Response(_contactService.GetAll());
        }
        [Route("contacts")]
        [HttpPost]
        public IActionResult Post([FromBody]ContactViewModel contactViewModel)
        {
            if (!ModelState.IsValid)
            {
                NotifyModelStateErrors();
                return Response(contactViewModel);
            }

            _contactService.Add(contactViewModel);

            return Response(contactViewModel);
        }
        [Route("contacts")]
        [HttpDelete]
        public IActionResult Delete(Guid id)
        {
            _contactService.Delete(id);
            return Response();
        }
    }
}