using System;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using W12.Assessment.Matheus.Application.AutoMapper;

namespace W12.Assesment.Matheus.WebAPI.Configuration
{
    public static class AutoMapperSetup
  {
      public static void AddAutoMapperSetup(this IServiceCollection services)
      {
          if (services == null) throw new ArgumentNullException(nameof(services));

          services.AddAutoMapper(); 
          AutoMapperConfig.RegisterMappings();
      }
  }
}