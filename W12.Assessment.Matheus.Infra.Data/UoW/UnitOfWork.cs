﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using W12.Assessment.Matheus.Domain.Core.Commands;
using W12.Assessment.Matheus.Domain.Interfaces;
using W12.Assessment.Matheus.Infra.Data.Context;

namespace W12.Assessment.Matheus.Infra.Data.UoW
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly W12Context _context;
        public UnitOfWork(W12Context context)
        {
            _context = context;
        }
        public CommandResponse Commit()
        {
            var rowsAffected = _context.SaveChanges();
            return new CommandResponse(rowsAffected > 0);
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
