﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using W12.Assessment.Matheus.Domain.Core.Entities;
using W12.Assessment.Matheus.Domain.Interfaces;
using W12.Assessment.Matheus.Infra.Data.Context;

namespace W12.Assessment.Matheus.Infra.Data.Repository
{
    public class Repository<T> : IRepository<T> where T : Entity
    {
        protected readonly W12Context Db;
        protected readonly DbSet<T> DbSet;
        public Repository(W12Context db)
        {
            Db = db;
            DbSet = Db.Set<T>();
        }
        public void Add(T obj)
        {
            DbSet.Add(obj);
        }
        public IQueryable<T> GetAll()
        {
            return DbSet;
        }
        public void Delete(Guid id)
        {
            DbSet.Remove(DbSet.Find(id));
        }

        public int SaveChanges()
        {
            return Db.SaveChanges();
        }

        public void Dispose()
        {
            Db.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
