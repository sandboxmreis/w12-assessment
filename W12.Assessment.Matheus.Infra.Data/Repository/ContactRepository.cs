﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using W12.Assessment.Matheus.Domain.Entities;
using W12.Assessment.Matheus.Domain.Interfaces;
using W12.Assessment.Matheus.Infra.Data.Context;

namespace W12.Assessment.Matheus.Infra.Data.Repository
{
    public class ContactRepository : Repository<Contact>,IContactRepository
    {
        public ContactRepository(W12Context context):base(context)
        {

        }
    }
}
