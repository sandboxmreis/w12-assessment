W12 .net senior developer assessment
====================
Considerações
---------------------
1. Tomei a liberdade de fazer com asp.net core 2.0.
2. Como era apenas 1 entidade, implementei um CQRS reduzido, apenas com Command e Query.
3. Usei Swagger para o mapeamento da API.
4. Usei angularJS 4 e deixei back e front no mesmo repositório para facilitar a avalaição e compartilhamento do código.