﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using W12.Assessment.Matheus.Application.ViewModels;

namespace W12.Assessment.Matheus.Application.Interfaces
{
    public interface IContactService
    {
        void Add(ContactViewModel contactVM);
        IEnumerable<ContactViewModel> GetAll();
        void Delete(Guid id);
    }
}
