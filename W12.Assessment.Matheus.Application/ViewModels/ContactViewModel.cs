﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace W12.Assessment.Matheus.Application.ViewModels
{
    public class ContactViewModel
    {
        public Guid Id { get; set; }
        public string Name { get;   set; }
        public string Telephone { get;   set; }
    }
}
