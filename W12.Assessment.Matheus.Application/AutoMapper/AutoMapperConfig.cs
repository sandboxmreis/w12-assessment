﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace W12.Assessment.Matheus.Application.AutoMapper
{
    public class AutoMapperConfig
    {
        public static MapperConfiguration RegisterMappings()
        {
            return new MapperConfiguration(c =>
            {
                c.AddProfile(new DomainToViewModelMappingProfile());
                c.AddProfile(new ViewModelToDomainMappingProfile());
            });
        }
    }
}
