﻿using AutoMapper;
using W12.Assessment.Matheus.Application.ViewModels;
using W12.Assessment.Matheus.Domain.Commands;

namespace W12.Assessment.Matheus.Application.AutoMapper
{
    public  class ViewModelToDomainMappingProfile : Profile
    {
        public ViewModelToDomainMappingProfile()
        {
            CreateMap<ContactViewModel, CreateContactCommand>()
                .ConstructUsing(c => new CreateContactCommand(c.Name,c.Telephone));
        }
    }
}
