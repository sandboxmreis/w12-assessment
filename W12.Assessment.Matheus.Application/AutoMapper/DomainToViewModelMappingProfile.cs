﻿using W12.Assessment.Matheus.Application.ViewModels;
using W12.Assessment.Matheus.Domain.Entities;
using AutoMapper;

namespace W12.Assessment.Matheus.Application.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            CreateMap<Contact, ContactViewModel>();

        }
    }
}
