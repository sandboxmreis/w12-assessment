﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using W12.Assessment.Matheus.Application.Interfaces;
using W12.Assessment.Matheus.Application.ViewModels;
using W12.Assessment.Matheus.Domain.Commands;
using W12.Assessment.Matheus.Domain.Core.Bus;
using W12.Assessment.Matheus.Domain.Interfaces;

namespace W12.Assessment.Matheus.Application.Services
{
    public class ContactService : IContactService
    {
        private readonly IMapper _mapper;
        private readonly IContactRepository _contactRepository;
        private readonly IMediatorHandler Bus;

        public ContactService(IMapper mapper, IContactRepository contactRepository, IMediatorHandler bus)
        {
            _mapper = mapper;
            _contactRepository = contactRepository;
            Bus = bus;
        }

        public void Add(ContactViewModel contactVM)
        {
            var createCommand = _mapper.Map<CreateContactCommand>(contactVM);
            Bus.SendCommand(createCommand);
        }

        public IEnumerable<ContactViewModel> GetAll()
        {
            return _contactRepository.GetAll().ProjectTo<ContactViewModel>();

        }

        public void Delete(Guid id)
        {
            var deleteCommand=new DeleteContactCommand(id);
            Bus.SendCommand(deleteCommand);
        }
    }
}
